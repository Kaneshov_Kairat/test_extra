<?php

namespace App\Models;


use Illuminate\Support\Str;

class User
{
    public $id;
    public $level;
    public $history;

    public function __construct()
    {
        $this->id = Str::random(10);
        $this->level = 0;
        $this->history = [];
    }
}
