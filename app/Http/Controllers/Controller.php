<?php

namespace App\Http\Controllers;

use App\Models\Psychic;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request)
    {
        $psychics = json_decode(session('psychics'));
        if (empty($psychics)) {
            $count_psychic = random_int(2, 4);
            for ($i = 0; $i < $count_psychic; $i++) $psychics[] = new Psychic();

            $request->session()->put('psychics', json_encode($psychics));
        }
        dd($psychics);

        return view('welcome');
    }

    public function guessTheNumber(Request $request)
    {

    }
}
