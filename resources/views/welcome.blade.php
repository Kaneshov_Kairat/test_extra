<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Тестирование экстрасенсов</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <style>
        .content-wrapper{
            margin-left: 40px;
        }
    </style>
</head>
<body class="hold-transition skin-blue fixed">
<div class="wrapper">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Тестирование экстрасенсов
            </h1>
        </section>

        <section class="content" style="min-height: 0px;">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-solid">

                        <div class="box box-info">
                            <div class="box-header">

                            </div>
                            <div class="box-body">

                                <form method="POST" action="{{ url('/login') }}">
                                    {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label for="first_name">Загадайте двузначное число:</label>
                                                <input type="number" class="form-control" min="10" max="99"
                                                       name="number" value="{{ old('number') }}" required >
                                            </div>
                                        </div>
                                    <br>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <button type="submit" class="btn btn-primary">Отправить
                                                </button>
                                            </div>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
{{--<div class="login-box">--}}
{{--    <!-- /.login-logo -->--}}
{{--    <div class="login-box-body">--}}


{{--    </div>--}}
{{--    <!-- /.login-box-body -->--}}
{{--</div>--}}

</body>
</html>

